#pragma once
class TwiddleCalculator
{
public:
	TwiddleCalculator();
  /*calculate object skew
	inputs:
	filename - image path;
	outputs:
	return - error(1: calculation done, 0: can't load image, -1: calculation error);
	res - anticlockwise degrees;*/
	int TwiddleCalculator::Process(const char* filename, double& res);	
	~TwiddleCalculator();
private:
	IplImage* image;
	IplImage* imageToHough;
	IplImage* imageToShow;
	CvMemStorage* storage;
	CvSeq* houghRes;
	std::vector<double> tangents;
};