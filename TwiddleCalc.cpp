// TwiddleCalc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TwiddleCalculator.h"

int main(int argc, char* argv[])
{
	TwiddleCalculator* twiddleCalculator=new TwiddleCalculator(); 
	char* filename = argc >= 2 ? argv[1] : "Skew_Test1.png";	
	double res=0;
	int errNo=twiddleCalculator->Process(filename,res);
	delete twiddleCalculator;
	return 0;
}

