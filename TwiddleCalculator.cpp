#include "StdAfx.h"
#include "TwiddleCalculator.h"
using namespace std;
const int minLines=5;

TwiddleCalculator::TwiddleCalculator()
{
	image = 0;
	imageToHough=0;
	imageToShow=0;
	storage = cvCreateMemStorage(0);
	houghRes = 0;
}

int TwiddleCalculator::Process(const char* filename, double& res)
{
	image = cvLoadImage(filename, CV_LOAD_IMAGE_GRAYSCALE);
	if( !image )
	{
		cout<<"can't load image: "<<filename<<endl;
		return 0;
	}
	cout<<"open file: "<<filename<<endl;

	//�������� ������� �� ������ ����� � ������� �������������� ����
	CvSize size=cvGetSize(image);	
	imageToHough = cvCreateImage( size, 8, 1 );	
	cvCanny( image, imageToHough, 50, 200, 3 );
	houghRes = cvHoughLines2( imageToHough, storage, CV_HOUGH_PROBABILISTIC, 1, CV_PI/720, 70, 50, 10 );
    //Hough transform
	double dx=0;
	double dy=0;
	double dyAbs=0;
	double dxAbs=0;		
	int cVertRight=0;
	int cHorizRight=0;
	int cVertLeft=0;
	int cHorizLeft=0;
	imageToShow = cvCreateImage( size, 8, 3 );
	cvCvtColor( imageToHough, imageToShow, CV_GRAY2BGR );
	int countLines=houghRes->total;
	if (countLines<minLines) 
	{
		cout<<"No enouht lines"<<endl;
		return -1;
	}
	for(int i = 0; i < countLines; i++)
	{
		CvPoint* line = (CvPoint*)cvGetSeqElem(houghRes,i);

		/*tangents-�������� ���� ������� �����. ������� �� ������� ��������� �������� �������� 45 ����.
		��! ����� ������������, ��� ������������ ����� �� ��������������� ���������. ��� ����� �������, ����� ����� ������:
		������ � ���� dx<dy ��� ����� � ����� (dx>dy). ���� ���������,��� ������ ����� ������,
		������ ������ ����� �����������  � ��� ����� ������������ �� 90-x, ��� x-���� ��������, ���� �� ������ ����� �����.
		����� �������� ���� ������ ������� �������, � ��� ���� �����, ����� �� � 1-� (isRight) �������� ��� �� 2-� (isLeft).
		��� �������� ���� ������� ���������� ��� ����� (������ � tangents) ���, ����� ���������������� ����� ����� ���������� ���� �������*/

		dx=line[0].x-line[1].x;	
		dy=line[0].y-line[1].y;
		dxAbs=abs(dx);	
		dyAbs=abs(dy);
		bool isLeft=((dx>=0&&dy>=0)||(dx<=0&&dy<=0)); //� ������ ������� �� �������� ������ ���� �����������
		if (dxAbs<dyAbs)
		{
			if (dyAbs!=0)
				tangents.push_back(dxAbs/dyAbs);			
			if (isLeft)
				cVertLeft+=1;
			else 
				cVertRight+=1;
		}
		else
		{
			if (dxAbs!=0)
				tangents.push_back(dyAbs/dxAbs);		
			if (isLeft)
				cHorizLeft+=1;
			else 
				cHorizRight+=1;
		}	
		cvLine( imageToShow, line[0], line[1], CV_RGB(255,255,0));
	}

	//������� ������� ������������� ���������
	int medianIndex=tangents.size()/2-1;
	nth_element (tangents.begin(), tangents.begin()+medianIndex, tangents.end());
	//������ tangents[medianIndex]-�������
	
	//��������� ���� ������� 
	res=180/CV_PI*atan(tangents[medianIndex]);
	tangents.clear();

	//�������, ��� �������� ������. �������� ���� ������������ �������� ���� ����� �� 0 �� 180 ������ ������� �������
	if (cVertLeft+cVertRight>cHorizLeft+cHorizRight) 
	{
		res=90-res;
	}
	if (cVertLeft+cHorizLeft>cVertRight+cHorizRight)
	{
		res=180-res;
	}
	cout<<"anticlockwise degrees = "<<res;
	cvNamedWindow( "Result",CV_WINDOW_AUTOSIZE);
	cvShowImage( "Result", imageToShow );	
	cvWaitKey(0);
	return 1;
}

TwiddleCalculator::~TwiddleCalculator()
{	
	cvReleaseMemStorage(&storage);
	cvReleaseImage(&image);
	cvReleaseImage(&imageToHough);
	cvReleaseImage(&imageToShow);
	cvDestroyAllWindows();
}